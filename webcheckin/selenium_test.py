import os.path as osPath
from time import sleep
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from pathlib import Path
import yaml

data = yaml.safe_load(open('test.yaml', 'r'))
print(data)

# Caminho para o Driver
driverDir = Path(osPath.abspath(osPath.curdir)
                        + '/../chromedriver_linux64/')

# Caminho para o Binário do driver
driver = driverDir/'chromedriver'

""" Cria um serviço com o driver

Para ser controlado pela aplicação, pode ser parado,
iniciado...
"""
serv = Service(driver)

# Instancia o Chrome
chrome = Chrome(service=serv)

if __name__ == '__main__':
    # Chama página
    chrome.get('https://google.com/')

    # Barra de Pesquisa
    chrome.find_element(
        By.XPATH,
        '/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input'
    ).send_keys(data['nome'], Keys.RETURN)

    # Clique no link do google
    chrome.find_element(
        By.XPATH,
        '/html/body/div[7]/div/div[10]/div[1]/div[2]/div[2]/div/div/div[3]/div/div[1]/div/a/h3'
    ).click()

    # Pesquisa no instagram
    chrome.find_element(
        By.XPATH,
        '/html/body/div[1]/section/nav/div[2]/div/div/div[2]/input'
    ).send_keys('Evaristo')

    sleep(3)

    chrome.find_element(
        By.XPATH,
        '/html/body/div[1]/section/nav/div[2]/div/div/div[2]/input'
    ).send_keys(Keys.ENTER)

    # Id da janela atual
    curr_handle = chrome.current_window_handle

    # Criar nova janela
    sleep(2)
    chrome.switch_to.new_window('tab')
    chrome.get('https://seat.ind.br')

    sleep(4)

    # Voltar para a janela
    chrome.switch_to.window(curr_handle)

    chrome.find_element(
        By.XPATH,
        '/html/body/div[1]/section/main/div/div/div[1]/div[2]/form/div/div[1]/div/label/input'
    ).send_keys('TCHAU!', Keys.ENTER)
