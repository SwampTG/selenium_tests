import os
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

# Caminho para o Driver
driverDir = os.path.abspath(os.path.curdir) \
                + '/../chromedriver_linux64/'

# Caminho para o Binário do driver
driver = driverDir+'/chromedriver'

""" Cria um serviço com o driver

Para ser controlado pela aplicação, pode ser parado,
iniciado...
"""
serv = Service(driver)

options = ChromeOptions()
options.add_argument("no-sandbox")
options.add_argument("--disable-gpu")
options.add_argument("--window-size=800,600")
options.add_argument("--disable-dev-shm-usage")
options.add_argument('--headless')

if __name__ == '__main__':
    # Instancia o Chrome
    chrome = Chrome(service=serv, options=options)

    chrome.get('http://127.0.0.1:5000/')
    chrome.find_element(By.XPATH, '/html/body/form/input').send_keys('Teste 1')
    chrome.find_element(By.XPATH, '/html/body/form/button').click()
    chrome.close()