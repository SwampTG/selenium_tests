import os
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/', methods=['POST', 'GET'])
def post_home():
    if request.method == 'POST':
        file = open('data.txt', 'a')
        file.writelines(request.form.values())
        file.writelines(f'\n')
        file.close()
        return render_template('send.html')
    else:
        return render_template('send.html')
